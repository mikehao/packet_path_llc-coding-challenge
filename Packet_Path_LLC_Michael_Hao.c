#include <stdio.h>
#include <time.h>

void binary_compare(FILE *f1, FILE *f2) {
  char b1, b2;
  int flag = 1;
  int count = 0;

  /*iterate through files and check that characters are equal*/
  while(((b1 = fgetc(f1)) != EOF) && ((b2 = fgetc(f2)) != EOF)) {
    count ++;
    /*return position if unequal and set flag to 0*/
    if(b1 != b2) {
      fseek(f1, 0, SEEK_CUR);
      flag = 0;
      break;
    }
  }

  /*if difference occurred*/
  if(flag == 0) {
    printf("Change occurs at character: %d\n", count);
    /*print 16 chars from file 1 if different*/
    for(int i = 0; i < 16; i++) {
      b1 = fgetc(f1);
      if(b1 == EOF) {
        return;
      }
      printf("%c ", b1);
    }
    printf("\n");
    /*print 16 chars from file 1 if different*/
    for(int i = 0; i < 16; i++) {
      b2 = fgetc(f2);
      if(b2 == EOF) {
        return;
      }
      printf("%c ", b2);
    }
    printf("\n");
  } else {
    /*flag is still 1, no changes*/
    printf("Changes are not present.\n");
  }
}

int main(int argc, char **argv) {
  FILE *fptr1, *fptr2;
  clock_t t;

  if(argc < 3) {
    printf("Files are missing!");
    return -1;
  }

  fptr1 = fopen(argv[1], "r");
  fptr2 = fopen(argv[2], "r");

  if(fptr1 == NULL || fptr2 == NULL) {
    printf("Both files are empty!");
    return -1;
  } else {
    t = clock();
    binary_compare(fptr1, fptr2);
    t = clock() - t;
  }
  double time_taken = ((double)t)/CLOCKS_PER_SEC;

  printf("Comparison time: %f", time_taken);
}
